function loadData() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let p = JSON.parse(this.responseText);
      var ul = document.getElementById("list");

      for (x of p.results) {
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(x.name));
        ul.appendChild(li);
      }
    }
  };
  xhttp.open("GET", "https://swapi.dev/api/people/", true);
  xhttp.send();
}

function uploadData() {
  const data = {
    nombre: "Mario",
    apellido: "Zalazar",
    legajo: 76669,
    edad: 37,
  };
  const xhr = new XMLHttpRequest();
  xhr.open("POST", "https://httpbin.org/post");
  xhr.responseType = "json";
  xhr.send(JSON.stringify(data));
  xhr.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      console.log(xhr.response);
    }
  };
}
